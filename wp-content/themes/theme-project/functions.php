<?php
function loadThemeResources()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('style-project', get_template_directory_uri() . '/css/style-project.css');
    wp_enqueue_style('fonts', get_template_directory_uri() . '/css/fonts.css');

    wp_enqueue_script( 'jq', get_template_directory_uri() . '/js/jquery-2.1.4.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true );
}

add_action('wp_enqueue_scripts', 'loadThemeResources');


register_nav_menus(array(
    'primary' => __( 'Primary Menu' ),
    'responsive' => __( 'Responsive Menu' ),
    'footer-nav' => __( 'Footer Menu' )
));
add_action ( 'init', 'ts_register_theme_post_types' );

function ts_register_theme_post_types() {
    register_post_type ( 'workshops', array (
        'labels' => array (
            'name' => __ ('Workshops'),
            'singular_name' => __ ('workshops'),

        ),
        'taxonomies' => array('category'),
        'public' => true,
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'page',
        'supports' => array (
            'title', 'thumbnail'
        )
    ) );
}

add_filter('excerpt_length', function() {
    return 20;
});

add_filter('excerpt_more', function() {
    return '..';
});
