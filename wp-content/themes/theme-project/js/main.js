$(function () {
    var currentMenuHoverItems = {},
        currentMenuHoverlist = {},
        catName,
        subMenuImages,
        eventMenuBut = $('.hover-menu'),
        childMenu = $('.child-menu');

    eventMenuBut.hover(
        function () {
            catName = $(this).find('a').attr('title');
            currentMenuHoverlist.item = childMenu.find('[data-sub-menu="' + catName + '"]');
            currentMenuHoverlist.item.addClass('z-in-120').fadeIn(300);
            if (currentMenuHoverlist.item !== currentMenuHoverlist.prevItem) {
                currentMenuHoverlist.prevItem = currentMenuHoverlist.item;
            }
            if (currentMenuHoverItems.length) {
                currentMenuHoverItems.img.show();
            }
        },
        function () {
            if ($('.child-menu-item:hover').length != 0) {
                currentMenuHoverlist.item.hover(
                function () {},
                function () {
                    currentMenuHoverlist.item.removeClass('z-in-120');
                    currentMenuHoverlist.prevItem.fadeOut(400);
                })
            } else {
                currentMenuHoverlist.item.removeClass('z-in-120');
                currentMenuHoverlist.prevItem.fadeOut(400);
            }
        }
    );

     $('li.child-menu-list').hover(
         function () {
             subMenuImages = $(this).parent().parent().siblings('.child-menu-images');
             subMenuImages.find('img').hide();
             currentMenuHoverItems.itemIndex = $(this).attr('data-list-hover-item');
             currentMenuHoverItems.img = subMenuImages.find('img[data-img-hover-item="' + currentMenuHoverItems.itemIndex + '"]');
             currentMenuHoverItems.img.show();
         },
        function () {}
    );

    ////////////////////////////////////////////menu
    $('#nav-toggle').on('click', function () {
        $('.resp-menu').stop().slideToggle();
        $(this).toggleClass('on')
    });

    $('.resp-sub > a').prepend('<i class="double-down"></i>');

    $('.resp-sub').on('click', function () {
        if($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).find('.sub-menu').stop().slideUp();
        } else {
            $('.sub-menu').stop().slideUp();
            $(this).find('.sub-menu').stop().slideDown();
            $('.resp-sub').removeClass('open');
            $(this).addClass('open');
        }

    });

    function setIdProduct() {
        var productId = $(".product-id").text(),
            productTitle = $(".single-caption").text(),
            productTitleInput = $("[name='your-title']"),
            productIdInput = $("[name='your-id']");

            productTitleInput.val(productTitle);
            productIdInput.val(productId);
    }

    $('input[type="submit"]').on('click', function(){
        $('.wpcf7-response-output').addClass('form-errors');



    });

    $('input').on('focus', function(){
        $(this).siblings('span').remove();
    });
    $('textarea').on('focus', function(){
        $(this).siblings('span').remove();
    });

    $('textarea, input').on('blur', function(){
        if($(this).val() !==  ''){
            $(this).removeClass('wpcf7-not-valid')
        }
    });





    ////////////////////////////////////////
    var popup =  $('.popup-box');
    $('.enquiry-button').on('click', function(){
        setIdProduct();
        popup.fadeIn(200);
        $('body').addClass('hidden-body');
    });
    $('.close').on('click', function(){
        popup.fadeOut(200);
        $('body').removeClass('hidden-body');
    });
    $(document).on('click touchstart', '.popup-box', function (event) {
        if ($(event.target).closest('.popup-form').length) return;
        popup.fadeOut(200);
        event.preventDefault();
        $('body').removeClass('hidden-body');
    });

    //////////////////////////////
    var map = $('.contacts-map');
    map.click(function () {
        $('.contacts-map iframe').css("pointer-events", "auto");
    });

    map.mouseleave(function() {
        $('.contacts-map iframe').css("pointer-events", "none");
    });

});