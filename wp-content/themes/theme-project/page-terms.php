<?php get_header(); ?>
    <div class="content">
        <div class="terms-posts wrapper clearfix">
            <?php
                if (have_posts()) :
                    query_posts('p=289');
                    while (have_posts()) : the_post(); ?>
                        <div class="term-post-firs">
                            <p class="post-caption"><?php the_title(); ?></p>
                            <div class="post-content"><?php the_content(); ?></div>
                        </div>
                <?php
                    endwhile;
                endif;
            ?>
            <?php
            if (have_posts()) :
                query_posts('p=291');
                while (have_posts()) : the_post(); ?>
                    <div class="term-post-second">
                        <p class="post-caption"><?php the_title(); ?></p>
                        <div class="post-content"><?php the_content(); ?></div>
                    </div>
            <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
<?php get_footer(); ?>