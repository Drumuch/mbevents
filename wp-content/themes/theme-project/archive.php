<?php get_header(); ?>
<div class="content">
    <div class="content-post wrapper">

        <?php
        $args = array(
            'cat' => $cat,
            'posts_per_page' => -1,
            'order' => 'ASC'

        );
        $ads = new WP_QUERY($args);
        if ($ads->have_posts()) {
            while ($ads->have_posts()): $ads->the_post(); ?>

                <div class="product">
                    <div class="product-caption">
                        <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                    </div>
                    <div class="product-item">
                        <div class="product-image">
                            <div class="image">
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php the_field('image'); ?>" alt="">
                                </a>
                            </div>
                            <div class="product-link">
                                <a href="<?php the_permalink(); ?>">more</a>
                            </div>
                        </div>
                        <div class="squa"></div>
                    </div>
                    <div class="product-descrip">
                        <?php the_excerpt() ?>
                    </div>
                </div>
            <?php endwhile;
        } ?>

    </div>
</div>
<?php get_footer(); ?>

