<?php get_header(); ?>
<div class="content">
    <div class="single-post wrapper">
        <?php
        while(have_posts()) : the_post();
            ?>
            <div class="post-box first-box">
                <div class="single-image">
                    <img src="<?php the_field('image') ?>" alt="">
                </div>
            </div>
            <div class="post-box second-box">
                <span class="hidden product-id"><?php the_field('id_product')?></span>
                <p class="single-caption"><?php the_title()?></p>
                <div class="single-info text">
                    <?php the_field('information') ?>
                </div>
                <div class="single-content text">
                    <?php the_content() ?>
                </div>
            </div>
        <?php
            endwhile;
        ?>
        <div class="enquiry-form">
            <div class="enquiry-button">enquiry</div>
        </div>
        <div class="popup-box">
            <div class="popup-form">
                <div class="close"></div>
                <div class="contact7">
                    <?php echo do_shortcode('[contact-form-7 id="507" title="Main form"]');?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>


