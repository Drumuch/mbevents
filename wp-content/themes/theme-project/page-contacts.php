<?php get_header(); ?>
    <div class="content">
        <div class="contacts wrapper clearfix">
            <?php
            if (have_posts()) :
                query_posts('p=293');
                while (have_posts()) : the_post(); ?>
                    <div class="contacts-map"><?php the_content(); ?></div>
                    <?php
                endwhile;
            endif;
            ?>
                <?php
                if (have_posts()) :
                    query_posts('p=298');
                    while (have_posts()) : the_post(); ?>
                        <div class="contacts-info">
                            <p class="post-caption"><?php the_title(); ?></p>
                            <div class="post-content"><?php the_content(); ?></div>
                            <div class="post-info post-content"><?php the_field('information') ?></div>
                        </div>
                <?php
                    endwhile;
                endif;
                ?>
            	<div class="contacts-contact7 contact7">
                    <?php echo do_shortcode('[contact-form-7 id="507" title="Main form"]');?>
                </div>
        </div>
    </div>
<?php get_footer(); ?>