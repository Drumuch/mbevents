<div class="sharethis wrapper">
    <span class="share-text">Share with friends</span>
    <span class='st_facebook_large' displayText='Facebook'></span>
    <span class='st_twitter_large' displayText='Tweet'></span>
    <span class='st_linkedin_large' displayText='LinkedIn'></span>
    <span class='st_googleplus_large' displayText='Google +'></span>
</div>
<div class="footer clearfix">
    <div class="wrapper">
        <div class="social-icons">
            <?php echo do_shortcode('[wp_social_icons]') ?>
            <a href="">
                <span class="facebook"></span>
            </a>
            <a href=""></a>
            <a href=""></a>
        </div>
        <div class="copyright">
            <?php
            if (have_posts()) :
                query_posts('p=283');
                while (have_posts()) : the_post(); ?>
                    <div class="links"><?php the_content(); ?></div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
