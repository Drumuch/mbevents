    <?php get_header(); ?>
        <div class="content">
      <!--       <div class="custom-list-bg">
            <div class="custom-list wrapper">
                <?php
                if (have_posts()) :
                    query_posts(array(
                        'cat' => 48,
                        'orderby' => 'date',
                        'order' => 'ASC'
                    ));
                    while (have_posts()) : the_post(); ?>
                        <div class="item">
                            <p class="item-caption"><?php the_title(); ?></p>
                            <div class="item-content"><?php the_content(); ?></div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
                </div>
            </div> -->

            <div class="row wrapper">
                <?php
                if (have_posts()) :
                    query_posts(array(
                        'cat' => 49,
                        'orderby' => 'date',
                        'order' => 'ASC'
                    ));
                    while (have_posts()) : the_post(); ?>
                        <div class="row-product">
                            <p class="row-caption"><?php the_title(); ?></p>
                            <div class="row-image">
                                <img src="<?php the_field('image') ?>" alt="">
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>

            <div class="home-post wrapper">

                <?php
                if (have_posts()) :
                    query_posts('p=85');
                    while (have_posts()) : the_post(); ?>
                        <div class="post-list">
                            <p class="post-caption"><?php the_title(); ?></p>
                            <div class="post-content"><?php the_content(); ?></div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>

                <?php
                if (have_posts()) :
                    query_posts('p=87');
                    while (have_posts()) : the_post(); ?>
                        <div class="post-list">
                            <p class="post-caption"><?php the_title(); ?></p>
                            <div class="post-content"><?php the_content(); ?></div>
                            <div class="post-cat">

                                <div class="list-one">
                                    <?php
                                        $args = array(
                                            'child_of' => 38,
                                            'title_li' => ''
                                        );
                                    ?>
                                    <ul class="list-cat">
                                        <?php wp_list_categories( $args ); ?>
                                    </ul>
                                </div>
                                <div class="list-two">
                                    <?php
                                    $args = array(
                                        'child_of' => 21,
                                        'title_li' => ''
                                    );
                                    ?>
                                    <ul class="list-cat">
                                        <?php wp_list_categories( $args ); ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>

            </div>
        </div>
    <?php get_footer(); ?>

