<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon"  href="<?php echo get_template_directory_uri(); ?>/img/favicon.icon" />
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" />
    <title><?php bloginfo('name') ?></title>
    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({
            publisher: "bbbb3071-ec41-47f2-aba6-8ff743f8d219",
            doNotHash: false,
            doNotCopy: false,
            hashAddressBar: false
        });</script>

    <?php wp_head(); ?>
</head>
<body>

<div class="header">
    <div class="wrapper clearfix">
        <div class="logo logo-header"></div>

            <?php
            if (have_posts()) :
                query_posts('p=304');
                while (have_posts()) : the_post(); ?>
                    <div class="contacts-information"><?php the_content(); ?></div>
            <?php
                endwhile;
            endif;
                wp_reset_query();
            ?>

        <div id="nav-toggle">
            <div class="one"></div>
            <div class="two"></div>
            <div class="three"></div>
        </div>
    </div>
    <div class="primary-menu">
        <?php
        $args = array(
            'theme_location' => 'primary',
            'container_class' => ' wrapper nav-header'
        );
            wp_nav_menu($args);
        ?>
    </div>
    <div class="child-menu wrapper">
        <div data-sub-menu="menu-item-event" class="child-menu-item child-menu-event cf">
            <?php

            $catsId = [3,21,38,17,50];

            $counter = 0;
            $event_children = [];

            foreach ($catsId as $catId) {
                $categories = get_categories(array(
                    'parent' => $catId
                ));
                $event_children[$catId][] = $categories;
            }

            $id = $catsId[$counter];
            $current_cat = $event_children[$id][0];
            $l = count($current_cat);
            ?>
            <div class="child-menu-images">
                <?php
                for ($i = 0; $i < $l; $i++) {
                    echo '<img data-img-hover-item="' . $i . '" class="child-menu-image child-menu-image__item--' . $i . '" src="' . get_field('th_img', $current_cat[$i]) . '" alt="">';
                } ?>
            </div>
            <div class="child-menu-list">
                <ul>
                    <?php
                    for ($i = 0; $i < $l; $i++) {
                        $child = $current_cat[$i];
                        echo '<li data-list-hover-item="' . $i . '" class="child-menu-list child-menu-list__item--' . $i . '"><a href="' . get_category_link($child->term_id) . '">' . $child->name . '</a></li>';
                    } ?>
                </ul>
            </div>
            <?php $counter++; ?>
        </div>
        <div data-sub-menu="menu-item-action" class="child-menu-item child-menu-action cf">
            <?php
            $id = $catsId[$counter];
            $current_cat = $event_children[$id][0];
            $l = count($current_cat);
            ?>
            <div class="child-menu-images">
                <?php
                for ($i = 0; $i < $l; $i++) {
                    echo '<img data-img-hover-item="' . $i . '" class="child-menu-image child-menu-image__item--' . $i . '" src="' . get_field('th_img', $current_cat[$i]) . '" alt="">';
                } ?>
            </div>
            <div class="child-menu-list">
                <ul>
                    <?php
                    for ($i = 0; $i < $l; $i++) {
                        $child = $current_cat[$i];
                        echo '<li data-list-hover-item="' . $i . '" class="child-menu-list child-menu-list__item--' . $i . '"><a href="' . get_category_link($child->term_id) . '">' . $child->name . '</a></li>';
                    } ?>
                </ul>
            </div>
            <?php $counter++; ?>
        </div>
        <div data-sub-menu="menu-item-products" class="child-menu-item child-menu-products cf">
            <?php
            $id = $catsId[$counter];
            $current_cat = $event_children[$id][0];
            $l = count($current_cat);
            ?>
            <div class="child-menu-images">
                <?php
                for ($i = 0; $i < $l; $i++) {
                    echo '<img data-img-hover-item="' . $i . '" class="child-menu-image child-menu-image__item--' . $i . '" src="' . get_field('th_img', $current_cat[$i]) . '" alt="">';
                } ?>
            </div>
            <div class="child-menu-list">
                <ul>
                    <?php
                    for ($i = 0; $i < $l; $i++) {
                        $child = $current_cat[$i];
                        echo '<li data-list-hover-item="' . $i . '" class="child-menu-list child-menu-list__item--' . $i . '"><a href="' . get_category_link($child->term_id) . '">' . $child->name . '</a></li>';
                    } ?>
                </ul>
            </div>
            <?php $counter++; ?>
        </div>
        <div data-sub-menu="menu-item-wedding" class="child-menu-item child-menu-wedding cf">
            <?php
            $id = $catsId[$counter];
            $current_cat = $event_children[$id][0];
            $l = count($current_cat);
            ?>
            <div class="child-menu-images">
                <?php
                for ($i = 0; $i < $l; $i++) {
                    echo '<img data-img-hover-item="' . $i . '" class="child-menu-image child-menu-image__item--' . $i . '" src="' . get_field('th_img', $current_cat[$i]) . '" alt="">';
                } ?>
            </div>
            <div class="child-menu-list">
                <ul>
                    <?php
                    for ($i = 0; $i < $l; $i++) {
                        $child = $current_cat[$i];
                        echo '<li data-list-hover-item="' . $i . '" class="child-menu-list child-menu-list__item--' . $i . '"><a href="' . get_category_link($child->term_id) . '">' . $child->name . '</a></li>';
                    } ?>
                </ul>
            </div>
            <?php $counter++; ?>
        </div>
        <div data-sub-menu="menu-item-catering" class="child-menu-item child-menu-catering cf">
            <?php
            $id = $catsId[$counter];
            $current_cat = $event_children[$id][0];
            $l = count($current_cat);
            ?>
            <div class="child-menu-images">
                <?php
                for ($i = 0; $i < $l; $i++) {
                    echo '<img data-img-hover-item="' . $i . '" class="child-menu-image child-menu-image__item--' . $i . '" src="' . get_field('th_img', $current_cat[$i]) . '" alt="">';
                } ?>
            </div>
            <div class="child-menu-list">
                <ul>
                    <?php
                    for ($i = 0; $i < $l; $i++) {
                        $child = $current_cat[$i];
                        echo '<li data-list-hover-item="' . $i . '" class="child-menu-list child-menu-list__item--' . $i . '"><a href="' . get_category_link($child->term_id) . '">' . $child->name . '</a></li>';
                    } ?>
                </ul>
            </div>
            <?php $counter++; ?>
        </div>
    </div>
    <div class="responsive-menu">
        <?php
            $args = array(
                'theme_location' => 'responsive',
                'container_class' => 'container-menu2',
                'menu_class' => 'resp-menu',
                'menu_id' => 'resp-menu',

            );
            wp_nav_menu($args);
        ?>
    </div>
    <div class="banner">
        <?php

        $title = get_the_title();
        $args = array(
            'post_type' => 'workshops',
            'posts_per_page' => -1,
        );
        $ads = new WP_QUERY($args);
        if ($ads->have_posts()) {
            while ($ads->have_posts()): $ads->the_post(); ?>
                <div class="workshops-item">
                    <div class="image-header" style="background: url('<?php the_field('image') ?>') no-repeat 50% 100%;
                                                     background-size: cover">
                    </div>
                        <div class="img-descrip">
                            <div class="wrapper">
                                <h1>
                                    <?php
                                    if (is_front_page()){
                                        the_field('description');
                                    }
                                    elseif(is_category()){
                                            single_cat_title();
                                        }
                                    else{
                                        echo $title;
                                    }
                                    ?>
                                </h1>
                            </div>

                        </div>
                </div>
            <?php endwhile;
        } ?>
    </div>
</div>
